# README #

Steamの早期アクセスゲーム『[Rogalia](http://store.steampowered.com/app/528460/)』の有志による翻訳活動である[Rogalia 2ch翻訳所](https://docs.google.com/spreadsheets/d/1L50FkZL4E5ru8wyy7cQ-cma8FA989R_0cFGKabGWmIM)の翻訳データを読み取り、ゲームプログラムで利用可能な形式との相互変換機能を提供します。

## 使用方法 ##

### 設定方法 ###

1. [RogaliaJapaneseTranslation.gs](https://bitbucket.org/cockatiel/rogaliajapanesetranslation/src/d764207044f2bacc73bf9408a9845771e0412e33/RogaliaJapaneseTranslation.gs)　をダウンロードします。

2. [Rogalia 2ch翻訳所](https://docs.google.com/spreadsheets/d/1L50FkZL4E5ru8wyy7cQ-cma8FA989R_0cFGKabGWmIM) にアクセスします。

3. スプレッドシートの『ツール』メニューから、スクリプトエディタを押下し、スクリプトエディタを表示します。  
![step03-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/c911d311ca29c6cbb76beaaff0d3bf1032c8447d/img/step03-01.png "メニュー > スクリプトエディタ")  
![step03-02](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/c911d311ca29c6cbb76beaaff0d3bf1032c8447d/img/step03-02.png "スクリプトエディタ画面")

4. スクリプトエディタのコード入力箇所に、RogaliaJapaneseTranslation.gs の内容をコピー&ペーストします。  
![step04-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/c911d311ca29c6cbb76beaaff0d3bf1032c8447d/img/step04-01.png "スクリプトエディタへのコピー&ペースト")

5. スクリプトエディタ画面上で保存を行います。  
プロジェクト名の編集ダイアログが表示された場合、任意のプロジェクト名を入力し、OKを押下します。  
![step05-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/c911d311ca29c6cbb76beaaff0d3bf1032c8447d/img/step05-01.png "プロジェクト名の編集ダイアログ")

6. スプレッドシートのメニューの『ヘルプ』の右側に『Rogalia 日本語翻訳』といったメニュー項目が追加されていれば設定完了です。  
![step06-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/01a6255edba3d10a32ad4a60f72b38888666e31e/img/step06-01.png　"メニューへの Rogalia 日本語翻訳 の追加")

### 使い方 ###

1. スプレッドシートのメニューの『Rogalia 日本語翻訳』を押下します。  
![step07-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/01a6255edba3d10a32ad4a60f72b38888666e31e/img/step06-01.png "Rogalia 日本語翻訳メニュー")

2. 実行したい動作を選択し、押下します。  
![step08-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/c911d311ca29c6cbb76beaaff0d3bf1032c8447d/img/step08-01.png "メニュー項目")

3. 初めて実行をする場合、承認や権限の許可が求められますので、ご確認の上、許可を与えてください。  
![step09-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/c911d311ca29c6cbb76beaaff0d3bf1032c8447d/img/step09-01.png "承認ウィンドウ")  
![step09-02](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/01a6255edba3d10a32ad4a60f72b38888666e31e/img/step09-02.png "許可のリクエスト")

4. 処理が開始され、『スクリプトを実行しています』と表示されます。  
![step010-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/7e67dd895de2cdc8c5bcda123a2641a7bee446d8/img/step10-01.png "スクリプトを実行しています")

5. しばらくすると、『スクリプトを実行しています』という表示が消え、処理が完了します。  
作成されたファイルは、マイドライブ内の『RogaliaJapaneseTranslation』フォルダに保存されています。  
![step011-01](https://bytebucket.org/cockatiel/rogaliajapanesetranslation/raw/56af21b6692fc3395fe03f704fb705d55eb3bb8d/img/step11-01.png "マイドライブへのファイルの出力")