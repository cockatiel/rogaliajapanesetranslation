var projectName = "RogaliaJapaneseTranslation";

function onOpen() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  
  var entries = [
    {
      name : "\"日本語翻訳所\" シートを dict.js としてエクスポート",
      functionName : "exportDictJS"
    },
    {
      name : "\"アイテム説明文\" シートを items.js としてエクスポート",
      functionName : "exportItemsJS"
    },
    {
      name : "dict.js を \"日本語翻訳所\" シートにインポート",
      functionName : "importDictJSFromRepos"
    },
    {
      name : "items.js を \"アイテム説明文\" シートにインポート",
      functionName : "importItemsJSFromRepos"
    },
  ];
    
  spreadsheet.addMenu("Rogalia 日本語翻訳", entries);
};


/**
* "日本語翻訳所" シートを読み取り dict.js と logファイル を出力します。
* 出力ファイル名は『dict_yyyyMMdd_HHmmss.js』、
* logファイル名は『exportDictJS_log_yyyyMMdd_HHmmss.txt』となります。
*/
function exportDictJS() {
  var outputFileName = "dict";
  var logFileName = "exportDictJS_log";
  var sheetName = "日本語翻訳所";
  var variableName = "dict";
  var headerRowIndex = 1;
  var originalColumnIndex = 2;
  var japaneseColumnIndex = 4;
  
  RogaliaJapaneseTranslation.exportRogaliaLangFileFromSheet(outputFileName,
                                                           logFileName,
                                                           sheetName,
                                                           variableName,
                                                           headerRowIndex,
                                                           originalColumnIndex,
                                                           japaneseColumnIndex);
};

/**
* "アイテム説明文" シートを読み取り items.js と logファイル を出力します。
* 出力ファイル名は『items_yyyyMMdd_HHmmss.js』、
* logファイル名は『exportItemsJS_log_yyyyMMdd_HHmmss.txt』となります。
*/
function exportItemsJS() {
  var outputFileName = "items";
  var logFileName = "exportItemsJS_log.txt";
  var sheetName = "アイテム説明文";
  var variableName = "items";
  var headerRowIndex = 1;
  var originalColumnIndex = 1;
  var japaneseColumnIndex = 5;
  
  RogaliaJapaneseTranslation.exportRogaliaLangFileFromSheet(outputFileName,
                                                           logFileName,
                                                           sheetName,
                                                           variableName,
                                                           headerRowIndex,
                                                           originalColumnIndex,
                                                           japaneseColumnIndex);
};

/**
* Rogalia レポジトリの dict.js を"日本語翻訳所" シートにインポートし、logファイル を出力します。
* logファイル名は『importDictJS_log_yyyyMMdd_HHmmss.txt』となります。
*/
function importDictJSFromRepos() {
  var inputUrl = "https://raw.githubusercontent.com/TatriX/rogalia/master/src/lang/ja/dict.js";
  var logFileName = "importDictJS_log";
  var sheetName = "日本語翻訳所";
  var variableName = "dict";
  var headerRowIndex = 1;
  var numberColumnIndex = 1;
  var originalColumnIndex = 2;
  var japaneseColumnIndex = 3;
    
  var content = RogaliaJapaneseTranslation.getFileFromUrl(inputUrl);
  if (content !== null) {
    var regexp = new RegExp("T\." + variableName + "\\s*?=\\s*?({[\\s\\S]+?});", "m");
    var match = regexp.exec(content);
    
    if (match.length > 0) {
        var data = JSON.parse(match[1]);
        RogaliaJapaneseTranslation.importSheetFromRogaliaLangFile(logFileName,
                                                                  sheetName,
                                                                  data,
                                                                  headerRowIndex,
                                                                  numberColumnIndex,
                                                                  originalColumnIndex,
                                                                  japaneseColumnIndex);
    }
  }
};

/**
* Rogalia レポジトリの items.js を"アイテム説明文" シートにインポートし、logファイル を出力します。
* logファイル名は『importItemsJS_log_yyyyMMdd_HHmmss.txt』となります。
*/
function importItemsJSFromRepos() {
  var inputUrl = "https://raw.githubusercontent.com/TatriX/rogalia/master/src/lang/ja/items.js";
  var logFileName = "importItemsJS_log";
  var sheetName = "アイテム説明文";
  var variableName = "items";
  var headerRowIndex = 1;
  var numberColumnIndex = 1;
  var originalColumnIndex = 2;
  var japaneseColumnIndex = 4;
  
  var content = RogaliaJapaneseTranslation.getFileFromUrl(inputUrl);
  if (content !== null) {
    var regexp = new RegExp("T\." + variableName + "\\s*?=\\s*?({[\\s\\S]+?});", "m");
    var match = regexp.exec(content);
   
    if (match.length > 0) {
      var data = JSON.parse(match[1]);
      RogaliaJapaneseTranslation.importSheetFromRogaliaLangFile(logFileName,
                                                                sheetName,
                                                                data,
                                                                headerRowIndex,
                                                                numberColumnIndex,
                                                                originalColumnIndex,
                                                                japaneseColumnIndex);
    }
  }
};
    
var RogaliaJapaneseTranslation = {
  /**
  * Rogalia で使用されている Langファイル の実際の出力処理です。
  *
  * @param {String} outputFileName        出力するファイル名のベース
  * @param {String} logFileName           logファイル名のベース
  * @param {String} sheetName             読み取りを行うシート名
  * @param {String} variableName          出力されるLangファイル内で使用されている変数名
  * @param {Number} headerRowIndex        読み取りを行うシートの見出しの行番号(見出しが複数行に渡る場合は一番最後の行)
  * @param {Number} originalColumnIndex   原文の列番号
  * @param {Number} japaneseColumnIndex   和訳の列番号
  */
  exportRogaliaLangFileFromSheet: function(outputFileName,
                                           logFileName,
                                           sheetName,
                                           variableName,
                                           headerRowIndex,
                                           originalColumnIndex,
                                           japaneseColumnIndex) {
    var activeSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = activeSpreadSheet.getSheetByName(sheetName);
    if (sheet == null) {
      Browser.msgBox("\"" + sheetName + "\" Sheet を取得できませんでした。");
      return;
    }
    
    var yMax = sheet.getLastRow() - 1;
    var values = sheet.getDataRange().getValues();
    var original;
    var japanese;
    var translated = {};
    var log = "begin export " + variableName + " at " + Utilities.formatDate(new Date() , 'Asia/Tokyo' , 'yyyy/MM/dd HH-mm-ss') + "\n";
    for (var y = headerRowIndex; y <= yMax; y = y + 1) {
      original = values[y][originalColumnIndex - 1];
      japanese = values[y][japaneseColumnIndex - 1];
      
      if (original == "") {
        log = log + y + ": 原文が空白のため、出力結果に追加を行いませんでした。\n";
        continue;
      }
      
      if (translated.hasOwnProperty(original)) {
        log = log + y + ": 原文 \"" + original + "\" が重複しているため、出力結果に追加を行いませんでした。\n";
        continue;
      }
      
      translated[original] = japanese;
    }
    
    var outputText = "\"use strict\";\n\n" +
      "T." + variableName + " = " + JSON.stringify(translated, null , "\t")
      + ";";
    
    var dateString = Utilities.formatDate(new Date() , 'Asia/Tokyo' , 'yyyyMMdd_HHmmss');
    RogaliaJapaneseTranslation.output(outputFileName + "_" + dateString + ".js", outputText);
    if (log != "") {
      log = log + "end export " + variableName + " at " + Utilities.formatDate(new Date() , 'Asia/Tokyo' , 'yyyy/MM/dd HH-mm-ss');
      RogaliaJapaneseTranslation.output(logFileName + "_" + dateString + ".txt", log);
    }
  },

  /**
  * ファイル出力の実際の処理です。
  *
  * @param {String} fileName        出力するファイル名
  * @param {String} outputText      出力するテキスト
  */
  output: function(fileName, outputText) {
    var folders = DriveApp.getRootFolder().getFolders();
    var destFolder = null;
    var folder;
    while(folders.hasNext()) {
      folder = folders.next();
      if (folder.getName() === projectName) destFolder = folder;
    }
    if (destFolder === null) {
      destFolder = DriveApp.createFolder(projectName);
    }
    
    var contentType = "text/plain";
    var charSet = "utf-8";
    var blob = Utilities.newBlob("", contentType, fileName).setDataFromString(outputText, charSet);
    
    destFolder.addFile(DriveApp.createFile(blob));
  },
  
  /**
  * Rogalia で使用されている Langファイル をリポジトリから取得し、スプレッドシートに反映します。
  *
  * @param {String} logFileName           logファイル名のベース
  * @param {String} sheetName             書き込みを行うシート名
  * @param {Object} data                  リポジトリからの取得データ
  * @param {Number} headerRowIndex        読み取りを行うシートの見出しの行番号(見出しが複数行に渡る場合は一番最後の行)
  * @param {Number} numberColumnIndex     連番の列番号
  * @param {Number} originalColumnIndex   原文の列番号
  * @param {Number} japaneseColumnIndex   和訳の列番号
  */
  importSheetFromRogaliaLangFile: function(logFileName,
                                           sheetName,
                                           data,
                                           headerRowIndex,
                                           numberColumnIndex,
                                           originalColumnIndex,
                                           japaneseColumnIndex) {
    var activeSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = activeSpreadSheet.getSheetByName(sheetName);
    if (sheet == null) {
      Browser.msgBox("\"" + sheetName + "\" Sheet を取得できませんでした。");
      return;
    }
    
    var log = "begin import to " + sheetName + " at " + Utilities.formatDate(new Date() , 'Asia/Tokyo' , 'yyyy/MM/dd HH-mm-ss') + "\n";
    var yMax = sheet.getLastRow() - 1;
    var values = sheet.getDataRange().getValues();
    var iMax = data.length;
    for(k in data){
      var index = RogaliaJapaneseTranslation.indexOf(values, originalColumnIndex - 1, k);
      if (index !== null) {
        if (values[index][japaneseColumnIndex - 1] !== data[k]) {
          log = log + (index + 1) + ": 原文 \"" + k + "\" の翻訳を \"" + values[index][japaneseColumnIndex - 1] + "\" から \"" + data[k] + "\" に変更しました。\n";
          values[index][japaneseColumnIndex - 1] = data[k];
        }
      }
      else {
        var jMax = values[0].length;
        var entry = [];
        for (var j = 0; j < jMax; j = j + 1) {
          entry[j] = ""; 
        }
        entry[numberColumnIndex - 1] = values.length;
        entry[originalColumnIndex - 1] = k;
        entry[japaneseColumnIndex - 1] = data[k];
        values.push(entry);
        log = log + values.length + ": 原文 \"" + k + "\" の翻訳 \"" + data[k] + "\" が追加となりました。\n";
      }
    }
    
    sheet.getRange(1, 1, values.length, values[0].length).setValues(values);
    
    var dateString = Utilities.formatDate(new Date() , 'Asia/Tokyo' , 'yyyyMMdd_HHmmss');
    if (log !== "") {
      log = log + "end export to " + sheetName + " at " + Utilities.formatDate(new Date() , 'Asia/Tokyo' , 'yyyy/MM/dd HH-mm-ss');
      RogaliaJapaneseTranslation.output(logFileName + "_" + dateString + ".txt", log);
    }
  },
  
  /**
  * 与えられた二次元配列、列番号、検索文字列を元に、検索文字列が発見された行を返します。
  * 
  * @param {Array} 2次元配列
  * @param {Number} 列番号
  * @param {String} 検索文字列
  */
  indexOf: function(array, columnIndex, value) {
    var iMax = array.length;
    var index = null;
    for (var i = 0; i < iMax; i = i + 1) {
      if (array[i][columnIndex] === value) {
        index = i;
        break;
      }
    }
    return index;
  },
  
  /**
  * 与えられたURLからファイルを取得し、内容を返します。
  * 
  * @param {String} ファイル取得先のURL
  */
  getFileFromUrl: function(url) {
    var content = null;
    try {
      var response = UrlFetchApp.fetch(url);
      content = response.getContentText("UTF-8");
    } catch (e) {
      Browser.msgBox("指定したURLのファイルの取得ができませんでした。");
    }
    
    return content;
  }
};